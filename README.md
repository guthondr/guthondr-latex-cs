Personal typographic and naming conventions of mathematical typesetting for computer science in LaTeX
==================================================================================================

- [view the conventions](https://gitlab.fit.cvut.cz/guthondr/guthondr-latex-cs/-/jobs/artifacts/master/browse?job=pdfLaTeX)

Author: Ondřej Guth (ondrej.guth@fit.cvut.cz)
